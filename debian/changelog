liburi-db-perl (0.23-1) unstable; urgency=medium

  * Import upstream version 0.23.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 19 Jan 2025 03:56:12 +0100

liburi-db-perl (0.22-1) unstable; urgency=medium

  * Import upstream version 0.22.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Apr 2024 00:02:06 +0200

liburi-db-perl (0.21-2) unstable; urgency=medium

  * Add "Breaks: sqitch (<< 1.3.1-2~)", as 1.3.1-2 is the first version to
    contain a patch fixing tests with newer versions of liburi-db-perl and
    liburi-perl.

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Jun 2023 19:35:11 +0200

liburi-db-perl (0.21-1) unstable; urgency=medium

  * Import upstream version 0.21.
  * Update years of upstream and packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-
    Browse. Changes-By: lintian-brush

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Jun 2023 03:05:08 +0200

liburi-db-perl (0.20-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + liburi-db-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 17:36:22 +0000

liburi-db-perl (0.20-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Update lintian overrides for renamed tags.

  [ Debian Janitor ]
  * Remove 1 unused lintian overrides.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Import upstream version 0.20.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sun, 26 Jun 2022 20:41:04 +0200

liburi-db-perl (0.19-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 01:02:43 +0100

liburi-db-perl (0.19-1) unstable; urgency=medium

  * Team upload.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Lucas Kanashiro ]
  * Import upstream version 0.19
  * Bump debhelper compatibility level to 11
  * Declare compliance with Debian Policy 4.1.5
  * Update years of upstream copyright

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 23 Jul 2018 01:28:11 -0300

liburi-db-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Import upstream version 0.18
  * Declare compliance with Debian policy 4.1.1
  * Update lintian override for warning from manpage due to long URI

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 12 Nov 2017 20:51:01 +0100

liburi-db-perl (0.17-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Lucas Kanashiro ]
  * Import upstream version 0.17
  * Declare compliance with Debian policy 3.9.7

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Thu, 04 Feb 2016 12:55:51 -0200

liburi-db-perl (0.16-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.16
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 06 Oct 2015 17:08:16 -0300

liburi-db-perl (0.15-2) unstable; urgency=medium

  * Team upload.
  * Add explicit build dependency on libmodule-build-perl
  * Declare the package autopkgtestable

 -- Niko Tyni <ntyni@debian.org>  Thu, 04 Jun 2015 23:21:38 +0300

liburi-db-perl (0.15-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Imported upstream version 0.15
  * Drop patch pod-whatis.patch; fixed upstream.

 -- gregor herrmann <gregoa@debian.org>  Wed, 17 Sep 2014 19:03:27 +0200

liburi-db-perl (0.13-1) unstable; urgency=medium

  * New upstream release.
  * Add patch to fix POD.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 18:00:11 +0200

liburi-db-perl (0.12-1) unstable; urgency=low

  * Initial release (closes: #755123).

 -- gregor herrmann <gregoa@debian.org>  Fri, 18 Jul 2014 14:21:26 +0200
